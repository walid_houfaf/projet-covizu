# Projet Covizu

Veuillez trouver au dessus l'ensemble de l'archive d'anonymisation du projet covizu, avec l'ensemble des scripts commentés, de la documentation, les fichiers CSV des bases de données, un dump sql à restaurer dans pgAdmin et les résultats produits par les méthodes d'anonymisation.

Pour des questions sur la réalisation de ce travail ou l'utilisation de cette archive, vous pouvez me contacter par mail à l'adresse: walid.houfaf-khoufaf@ensg.eu 

