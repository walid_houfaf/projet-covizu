# -*- coding: utf-8 -*-
"""
Created on Fri Aug 14 15:52:01 2020

@author: houfa
"""

import csv
import math
import random
import numpy as np
import psycopg2

conn = psycopg2.connect(user="postgres", password="postgres", database="donnees_hospitalieres_COVID19", host="localhost", port="5432")

#on lit le fichier des données anonymisées par simulated crowding
b=open('simulated_crowding75v2_3.csv')
dataCSVS=csv.reader(b)
listdataCSVS=list(dataCSVS)

#on lit le fichier des données non-anonymisées avec leur disposition dans les clusters
c=open('donnees_detec_clusterna.csv')
dataCSV=csv.reader(c)
listdataCSV=list(dataCSV)


def kano(xa,ya,xb,yb):
    #fonction retournant la valeur de la k-anonymité d'un couple de points non-anonymisé et anonymisé
    cursorK=conn.cursor()
    queryK="Select count(*) from public.bano7593 as b WHERE ST_DWithin(b.geom,ST_SetSRID(ST_Point(%s,%s),2154),ST_Distance(ST_SetSRID(ST_Point(%s,%s),2154),ST_SetSRID(ST_Point(%s,%s),2154)))" 
    cursorK.execute(queryK,(xa,ya,xa,ya,xb,yb))
    recordsK= cursorK.fetchall()
    return(int(recordsK[0][0]))

def dista(xa,ya,xb,yb):
    #fonction retournant la distance euclidienne d'un couple de points
    n=math.sqrt(((xa-xb)**2)+((ya-yb)**2))
    return(n)
data=[]
bruit=[]
appari=[]

for k in range(1,len(listdataCSV)):
    #on sépare dans deux listes, les points appartenant à un cluster et ceux du 'bruit'
    if int(listdataCSV[k][12])==(-1):
        bruit.append(listdataCSV[k])
    else:
        appari.append(listdataCSV[k])

for k in range(1,len(listdataCSVS)):
    d=[]
    ka=0
    e=[]
    if int(listdataCSVS[k][11])==-1:
        #pour les points appartenant au bruit, on calcule la k-anonymité avec le point non anonymisé de même identifiant
        for i in range(0,len(bruit)):
            if int(listdataCSVS[k][0])==int(bruit[i][0]):
                ka=kano(float(listdataCSVS[k][9]),float(listdataCSVS[k][10]),float(bruit[i][9]),float(bruit[i][10]))
                e=bruit[i]
        d.append(e[0])
        d.append(e[1])
        d.append(e[2])
        d.append(e[3])
        d.append(e[4])
        d.append(e[5])
        d.append(e[6])
        d.append(e[7])
        d.append(e[8])
        d.append(e[9])
        d.append(e[10])
        d.append(ka)

    else:
        #pour les points appartenant à des clusters étant donné qu'ils n'ont plus d'identifiant, on apparie avec le point le plus proche pour calculer la k-anonymité
        #un point de la base non anonymisé ne peut être apparié qu'une seule fois, si le point selectionné a déjà été apparié on va cherche le deuxième plus proche et ainsi de suite jusqu'à avoir un point disponible
        for m in range(0,len(appari)):
            if appari[m][12]!=-1:
                min=dista(float(listdataCSVS[k][9]),float(listdataCSVS[k][10]),float(appari[m][9]),float(appari[m][10]))
                e=appari[m]
                break
        if len(appari)>1:
            for j in range(1,len(appari)):
                if dista(float(listdataCSVS[k][9]),float(listdataCSVS[k][10]),float(appari[j][9]),float(appari[j][10]))<min and appari[j][12]!=-1:
                    min=dista(float(listdataCSVS[k][9]),float(listdataCSVS[k][10]),float(appari[j][9]),float(appari[j][10]))
                    e=appari[j]
        ka=kano(float(listdataCSVS[k][9]),float(listdataCSVS[k][10]),float(e[9]),float(e[10]))
        d.append(e[0])
        d.append(e[1])
        d.append(e[2])
        d.append(e[3])
        d.append(e[4])
        d.append(e[5])
        d.append(e[6])
        d.append(e[7])
        d.append(e[8])
        d.append(e[9])
        d.append(e[10])
        d.append(ka) 
        for l in range(0,len(appari)):
            #on enlève de la base au fur et à mesure les points non anonymisés qui viennent d'être appariés
            if int(appari[l][0])==int(e[0]):
                appari.pop(l)
                break
    data.append(d)
    print(d)
    

#en sortie on a un fichier CSV avec les points de la base de données non anonymisées et la valeur de k qui leur ai associé
with open('result_k_crowding75.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["id","identifiant","numero","adresse","postal","commune","source","geom","date","x","y","k"])
    for i in range(len(data)):
        writer.writerow(data[i])